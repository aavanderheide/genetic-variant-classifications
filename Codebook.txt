abbreviation		Class			full name
1	CLASS		num			The binary representation of the target class. 0 represents no conflicting submissions and 1 represents conflicting
2	Consequence	Factor w/ 48 levels	Type of consequence
3	IMPACT  	Factor w/ 4 levels	The impact modifier for the consequence type
4	SIFT    	Factor w/ 5 levels	The SIFT prediction and/or score, with both given as prediction(score)
5	PolyPhen	Factor w/ 5 levels	The PolyPhen prediction and/or score

