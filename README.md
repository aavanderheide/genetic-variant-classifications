# Genetic Variant Classifications #

Author: Aaron van der Heide.  
Version: 1.0  
Date: 16-11-2020  
Student Bioinformatics at Hanzehogeschool Groningen.  

### Repository info ###

This repository contains the log and all its files. To see what is going on here please open the DefiniteLogAndReport folder and from there open the Aaron_vd_Heide_Th9_CompleteLog.Rmd or .pdf.
The names of the files introduce themselves.